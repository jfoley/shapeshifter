/*
 *  This file contains code for rendering and manipulating the gather view,
 *  where resources are accumulated, modified, and displayed
 */


var GatherView = (function() { 
  var onClickTermSuggest = function(evt) {
    SearchAction.addTermAndSearch(evt.target.innerHTML);
  };

  return {
    display: function() {
      workspace.empty();

      // sort by identifier -- will group same books together
      _(project.contents).sortBy(function(obj) {
        return obj.identifier;
      }).each(function(result) {
        workspace.append(
                '<div class="' + result.type + '">' +
                '<strong>' + result.shortTitle + '</strong> ' +
                TypeFormat.shortStr(result) +
                '</div>'
                );
      });

      workspace.append('<hr />');

      // generate suggested terms buttons
      // take all terms > 2 that aren't in STOPWORDS sorted by tf, and create a button
      _(project.posTermWeights).pairs().filter(function(tc) {
        return tc[1] > 2;
      }).reject(function(tc) {
        return _.contains(STOPWORDS, tc[0]);
      }).sortBy(function(tc) {
        return -tc[1];
      }).each(function(tc) {
        var term = tc[0];
        var button = $('<button>' + term + '</button>');
        button.click(onClickTermSuggest);
        workspace.append(button);
      });
    }
  };
}());
