var project = {
  contents: [],
  accepts: [],
  rejects: [],
  posTermWeights: {},
  negTermWeights: {}
};

var GatherState = (function() {
  var judgeCount = function() {
    return _.size(project.accepts) + _.size(project.rejects);
  };
  
  // add the count of all unigrams to target
  var processText = function(text, target) {
    // tokenize, build Map[term, count]
    _.each(TextProcessor.split(text), function(term) {
      accum(target, term, 1);
    });
  };
  
  var accept = function(obj) {
    project.contents.push(obj);
    project.accepts.push(obj.identifier);

    // build up positive word cloud
    processText(obj.text, project.posTermWeights);
  };
  
  var reject = function(obj) {
    project.rejects.push(obj.identifier); // keep around the identifier to filter results
    // build up negative word cloud
    processText(obj.text, project.negTermWeights);
  };
  
  return {
    title: "President Abraham Lincoln",
    contents: [],
    accepts: [],
    rejects: [],
    posTermWeights: {},
    negTermWeights: {},
    init: function() {
      // TODO make an API call to load up this user's Gather?
      this.setTitle(this.title);
    },
    setTitle: function(newTitle) {
      $('#gatherName').html('Gather &quot;'+newTitle+'&quot;');
    },
    add: function(obj, rating) {
      if (rating > 0) {
        accept(obj);
      } else {
        reject(obj);
      }
    }
  };
}());