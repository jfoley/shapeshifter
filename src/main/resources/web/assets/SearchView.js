/*
 * This file contains all state pertaining to rendering the search view or the 
 * "right" side of the Gather demo.
 */

var SearchView = (function() {
  "use strict";
  
  // private module variables
  var foundObjects = [];

  /*
   * This method generates the feedback buttons in the results list.
   * 
   * TODO, generate an array of html objects, and add handlers here.
   */
  var feedbackButtons = function(id) {
    return _(["keep-perfect", "keep-okay", "reject-fair", "reject-terrible"]).reduce(function(result, rating) {
      var buttonVal = rating + ':' + id;
      return result + '<button value="' + buttonVal + '">' + rating + "</button>";
    }, "");
  };


  var highlight = function(text, highlightSet) {
    // alias module for easier reading
    var txt = TextProcessor;
    
    console.log(highlightSet);
    var hiwords = txt.eachWord(text, function (w) {
      var term = txt.clean(w);
      if(_.contains(highlightSet, term)) {
        return '<span class="hili">'+w+'</span>';
      }
      return w;
    });
    
    return _.reduce(hiwords, function (w1, w2) {
      // little hack to prevent extra spaces before periods
      if(txt.startsWithPunctuation(txt.dropTags(w2))) {
        return w1 + w2;
      }
      return w1 + ' ' + w2;
    }, "");
  };

  // turn a JSON result object into HTML
  var displayResult = function(obj) {
    var id = obj.identifier;

    obj.shortTitle = _.str.prune(obj.title, 40);
    console.log(obj.query);
    var qterms = TextProcessor.split(obj.query);
    console.log('post-split'+qterms);
    searchResults.append(
            '<div id="' + id + '">' +
            '<span class="title">' + highlight(_.str.prune(obj.title, 80), qterms) + '</span>' +
            '<div class="display">' +
            '<p>'+highlight(obj.text, qterms)+'</p>'+
            //'<p>' + obj.text + '</p>' +
            '</div>' + // .display
            '<div class="feedback">' + feedbackButtons(id) + '</div>' +
            '</div>' // #id
            );
  };

  var displaySearchResults = function(newObjs) {
    searchResults.empty();
    foundObjects = newObjs;
    _(foundObjects).each(displayResult);
  };

  /*
   * This method is called when a keep or reject button is called
   */
  var onClickFeedbackButton = function(evt) {
    var button = evt.target;
    var value = button.value;
    var action = value.split(':')[0];
    var id = value.split(':')[1];

    // find the corresponding object for this action
    var obj = _.find(foundObjects, function(obj) {
      return obj.identifier === id;
    });
    if (_.isUndefined(obj)) {
      throw new Error("Couldn't find object of id=" + id);
    }

    // remove it from the search results list
    $('#' + id).remove();

    if (action === 'keep-perfect') {
      obj.rating = 100;
    } else if (action === 'keep-okay') {
      obj.rating = 50;
    } else if (action === 'reject-fair') {
      obj.rating = -50;
    } else if (action === 'reject-terrible') {
      obj.rating = -100;
    } else {
      throw new Error("Unhandled action upon search result: " + action + " result: " + obj);
    }
    GatherState.add(obj, obj.rating);
    GatherView.display();
  };

// "public" functions
  return {
    addResults: function(newObjs) {

      displaySearchResults(newObjs);

      searchResults.append('<hr />');
      
      // create More button
      $('<button>').html('More').click(function() {
        SearchAction.onMoreButton();
      }).appendTo(searchResults);

      searchResults.find(".feedback button").click(onClickFeedbackButton);
    }
  };

}());