/*
 * This file contians all the relevant actions regarding the SearchView, 
 * all the appropriate, public interactions.
 * 
 */

var SearchAction = (function() {
  var fireSearchEvent = function() {
    customSearchQuery.trigger("submit");
  };

  var onSubmitSearch = function(evt) {
    var query = TextProcessor.clean(customSearchQuery.val());
    if (query !== "") {
      doSearch(query, 4);
    }
    // don't refresh the page
    return false;
  };

  var filterResultsByProject = function(results) {
    return _.reject(results, function(obj) {
      var id = obj.identifier;
      // drop any pages the user has already added to the project
      return _.contains(project.rejects, id) || _.contains(project.accepts, id);
    });
  };

  
  var doSearch = function(query, count) {
    API.search("portland-pages", query, count+20, 0, function(jsonResponse) {
      var results = _.take(filterResultsByProject(jsonResponse.results), count);

      // mark up results
      _(results).map(function(res, rank) {
        res.query = jsonResponse.query;
        res.rank = rank;
      }).take(count);
      
      SearchView.addResults(results);
    });
  };

  return {
    init: function(initialQuery) {
      // set up box with search value
      customSearchQuery.val(initialQuery);
      customSearchForm.submit(onSubmitSearch);
      fireSearchEvent();
    },
    addTermAndSearch: function(newTerm) {
      var prevQuery = customSearchQuery.val()
      var prevQueryTerms = TextProcessor.split(prevQuery);
      if (_.contains(prevQueryTerms, newTerm)) {
        return;
      }
      customSearchQuery.val(prevQuery + ' ' + newTerm);
      fireSearchEvent();
    },
    onMoreButton: function() {
      fireSearchEvent();
      //TODO do something smart with offsets
    }
  };

}());