/*
 * This file contains all communcation points of the javascript front-end with 
 * the Java code in the server.
 * 
 * This should depend on nothing but ajax calls, and know nothing about the rest 
 * of the application or its data. Eventually, I want to build a queuing system 
 * in here for when intermittent internet connections fail.
 */

var API = (function() {
  
  return {
    search: function(type, query, count, skip, done_fn, error_fn) {
      assert(_.isFinite(skip), "api_search requires skip as number, found " + skip);
      assert(_.isFinite(count), "api_search requires count as number, found " + count);
      assert(_.isString(type), "api_search requires type as string, found " + type);
      assert(_.isString(query), "api_search requires query as string, found " + query);
      assert(_.isUndefined(done_fn) || _.isFunction(done_fn), "api_search requires done_fn as a function, found " + done_fn);
      assert(_.isUndefined(error_fn) || _.isFunction(error_fn), "api_search requires done_fn as a function, found " + error_fn);

      var request = {
        url: '/api/search.do?q=' + encodeURIComponent(query) + '&n=' + count + '&kind=' + type + '&skip=' + skip
      };
      if (_.isFunction(done_fn)) {
        request.success = done_fn;
      }
      if (!_.isFunction(error_fn)) {
        request.error = error_fn;
      }

      // send off request to the server
      $.ajax(request);
    }
  };
}());
