/*
 * This file exists because Javascript could really use a standard assert.
 */

// find out early if things aren't working
var assert = function(cond, msg) {
  if (!cond) {
    throw new Error(msg);
  }
};

// add a count to a map[k, int], treating undefined as zero
var accum = function(map, term, count) {
  if(_.isUndefined(map[term])) {
    map[term] = count;
  } else {
    map[term] += count;
  }
  return map;
};