/*
 * This is kind of like our javascript TagTokenizer, since we need to clean queries
 * and do some mild text-processing on the client side. We use regexes where 
 * possible because they are generally faster than equivalent DFAs written in JS
 * 
 * TODO need ability to Stem as well.
 * 
 * note: I know that was true once, but somebody ought to ask the internet / profile
 */

var TextProcessor = (function() {

  return {
    dropTags: function(text) {
      return _.str.stripTags(text.toLowerCase());
    },
    dropPunctuation: function(text) {
      return text.replace(/[^\w\s]|_/g, "");
    },
    cleanWhitespace: function(text) {
      return _.str.trim(text.replace(/\s+/g, " "));
    },
    splitToTerms: function(text) {
      console.log('splittoTerms '+text);
      return _.str.words(text);
    },
    dropEmpty: function(terms) {
      console.log('dropEmpty '+terms);
      var result = _.filter(terms, function(term) {
        return !term.match(/w+/);
      });
      console.log('dropEmpty. '+result);
      return result;
    },
            
    // simplest interface
    startsWithPunctuation: function(text) {
      return text.match(/^[^\w\s]|_/);
    },  
    clean: function(text) {
      return this.cleanWhitespace(this.dropPunctuation(this.dropTags((text))));
    },
    split: function(text) {
      return this.dropEmpty(this.splitToTerms(this.clean(text)));
    },
    eachWord: function(text, oper) {
      return _.map(this.splitToTerms(text)).map(oper);
    }
  };
}());