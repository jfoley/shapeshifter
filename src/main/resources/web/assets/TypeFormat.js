/*
 * This module contains any sort of string munging or image linking needed to 
 * display the various types. It knows all the dirty secrets about which things
 * are Internet Archive resources and whatnot.
 */

var TypeFormat = (function() {
  
  return {
    shortStr: function(obj) {
      if (!_.isUndefined(obj.pageNum)) {
        return 'pp. ' + obj.pageNum;
      }
      return '(' + obj.type + ')';
    }
  };
}());