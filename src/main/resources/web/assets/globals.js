// project dependencies
// jquery - for dom manipulation
// jquery-ui - for complex ui actions
// underscore - for javascript utility functions (_.isUndefined)
// underscore.string - for javascript string utility functions (_.clean, _.prune)


// dom objects; see the $(document).ready
var workspace;
var searchResults;
var customSearchForm, customSearchQuery;

//TODO load from inquery
/** @const */ var STOPWORDS = [
  "the", "a", "and", "of", "in", "to", "his", "he", "i", "that", "was", "by", 
  "as", "is", "from", "him", "for","has", "if", "but", "have", "its", "all", 
  "be", "with", "some", "two", "it", "not", "any", "quot", "which", "their", 
  "there", "are", "business", "so", "one", "you", "can", "had", "most", "time", 
  "we", "seen", "would", "could", "this"
];

// onload callback
$(document).ready(function() {
  
  // init global references to DOM
  workspace = $("#gatherSpace");
  searchResults = $("#searchResults");
  customSearchForm = $("#customSearch");
  customSearchQuery = $("#customSearchQuery");

  // init modules
  GatherState.init();
  SearchAction.init(GatherState.title);
});