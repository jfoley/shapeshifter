-- create the users table
create table if not exists users (
  id int not null auto_increment,
  name varchar(255) not null,
  primary key (id),
  unique (name)
  );

-- create the activeUsers table
create table if not exists activeUsers (
  id int not null, 
  session bigint not null,
  foreign key (id) references users(id)
  );

-- create the gather table
create table if not exists gathers (
  id int not null auto_increment,
  owner int not null,
  name varchar(255) not null,
  primary key (id),
  foreign key (owner) references users(id)
  );

-- create the gather contents table
create table if not exists gatherContents (
  gatherId int not null,
  type varchar(255),
  collection varchar(255),
  resource varchar(255),
  rating int not null,
  query text,
  foreign key (gatherId) references gathers(id)
  );

