package edu.umass.ciir.proteus.backend.galago;

import edu.umass.ciir.proteus.objects.BaseObject;
import edu.umass.ciir.proteus.objects.ObjectRetrieval;
import edu.umass.ciir.proteus.objects.ScoredObject;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.tupleflow.Parameters;

/**
 *
 * @author jfoley
 */
public abstract class GalagoObjectRetrieval extends ObjectRetrieval {
  Parameters sdmParms, queryParms;
  Retrieval retrieval;
  String indexPath;
    
  public GalagoObjectRetrieval(String indexPath) throws Exception {
    this.indexPath = indexPath;
    this.retrieval = new LocalRetrieval(indexPath, new Parameters());
        
    //--- these are standardish parameters for SDM
    //    TODO, someday we should tune these
    sdmParms = new Parameters();
    sdmParms.set("mu", 8000.0);
    sdmParms.set("odw", 0.21);
    sdmParms.set("uniw", 0.29);
    sdmParms.set("uww", 0.50);
    
    queryParms = new Parameters();
  }
  
  protected void logError(String msg, Exception ex) {
    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, msg, ex);
  }
  
  @Override
  public ArrayList<ScoredObject> search(String query, int numResults, int skip) {
    queryParms.set("requested", numResults+skip);
    
    //--- build sdm node tree
    String galagoQuery = toGalagoQuery(query);
    Node root = StructuredQuery.parse(galagoQuery);
    Node transformed;
    try {
      transformed = retrieval.transformQuery(root, sdmParms);
    } catch (Exception ex) {
      logError("Could not transform query `"+query+"'", ex);
      return ObjectRetrieval.NoResults;
    }
    
    //--- actually process query
    long start = System.currentTimeMillis();
    ScoredDocument[] results;
    try {
      results = retrieval.runQuery(transformed, queryParms);
    } catch (Exception ex) {
      logError("Could not runQuery `"+transformed+"'", ex);
      return ObjectRetrieval.NoResults;
    }
    long end = System.currentTimeMillis();
    System.out.println((end-start)+"ms. n="+numResults+" "+galagoQuery);
    
    //--- convert results to required format
    ArrayList<ScoredObject> rankedList = new ArrayList<ScoredObject>();
    if(results != null) {
      rankedList.ensureCapacity(numResults);
      // return only the subselection requested
      for(int i=0, rank=skip; i<numResults && rank < results.length; rank++, i++) { 
        ScoredDocument doc = results[rank];
        ScoredObject obj = new ScoredObject();
        obj.identifier = doc.documentName;
        obj.rank = rank;
        obj.score = doc.score;
        rankedList.add(obj);
      }
    }

    return rankedList;
  }

  @Override
  public abstract BaseObject fetch(String identifier);
  
  /**
   * Build up a Galago SDM query from raw input text.
   * 
   * For now, this casefolds, because TagTokenizer.
   * @param rawQuery text input
   * @return #sdm(terms...)
   */
  protected String toGalagoQuery(String rawQuery) {
    String[] cleanTerms = rawQuery.toLowerCase().replaceAll("[^a-z01-9]", " ").trim().split("\\s+");
    StringBuilder query = new StringBuilder();
    
    query.append("#sdm(");
    for(String term : cleanTerms) {
      if(term.length() > 0) {
        query.append(term).append(' ');
      }
    }
    query.append(")");
    
    return query.toString();
  }

}
