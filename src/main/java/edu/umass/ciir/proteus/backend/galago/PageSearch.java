package edu.umass.ciir.proteus.backend.galago;

import edu.umass.ciir.proteus.objects.BaseObject;
import edu.umass.ciir.proteus.objects.BookPage;
import java.io.IOException;

/**
 *
 * @author jfoley
 */
public class PageSearch extends GalagoObjectRetrieval {
  
  public PageSearch(String indexPath) throws Exception {
    super(indexPath);
  }

  @Override
  public BaseObject fetch(String identifier) {
    try {
      return new BookPage(identifier, retrieval);
    } catch (IOException ex) {
      logError("Couldn't retrieve document "+identifier+"!", ex);
      return null;
    }
  }

  @Override
  public String toString() {
    return "Galago Page Search over "+indexPath;
  }
}
