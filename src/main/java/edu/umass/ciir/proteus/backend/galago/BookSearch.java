package edu.umass.ciir.proteus.backend.galago;

import edu.umass.ciir.proteus.objects.BaseObject;
import edu.umass.ciir.proteus.objects.Book;
import java.io.IOException;

/**
 *
 * @author jfoley
 */
public class BookSearch extends GalagoObjectRetrieval {

  public BookSearch(String indexPath) throws Exception {
    super(indexPath);
  }
  
  @Override
  public BaseObject fetch(String identifier) {
    try {
      return new Book(identifier, retrieval);
    } catch (IOException ex) {
      logError("Couldn't retrieve document " + identifier + "!", ex);
      return null;
    }
  }
  
  @Override
  public String toString() {
    return "Galago Book Search over "+indexPath;
  }
}
