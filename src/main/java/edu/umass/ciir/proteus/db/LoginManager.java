package edu.umass.ciir.proteus.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class handles manipulating users - within the database.
 * @author jfoley
 */
public class LoginManager extends CommonSQL {
  private final Connection conn;
  
  public LoginManager(Connection conn) {
    this.conn = conn;
  }
  
  public int findUser(String userName) {
    ResultSet rs = null;
    Statement stmt = null;
    
    try {
      assert userName != null;
      stmt = conn.createStatement();
      stmt.executeQuery("select id from users where name='" + userName + "';");
      rs = stmt.getResultSet();

      if (rs.next()) {
        return rs.getInt("id");
      }
    } catch (SQLException ex) {
      fail("Couldn't attempt to find user ", ex);
    } finally {
      close(rs, stmt);
    }
    
    return -1;
  }

  public int createUser(String userName) {
    ResultSet rs = null;
    Statement stmt = null;
    
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate("insert into users (name) values ('" + userName + "');", Statement.RETURN_GENERATED_KEYS);
      rs = stmt.getGeneratedKeys();
      if(rs.next()) {
        return rs.getInt(1);
      }
    } catch (SQLException ex) {
      fail("Couldn't create user", ex);
    } finally {
      close(rs, stmt);
    }
    return -1;
  }

  public void loginUser(int userId, long sessionId) {
    Statement stmt = null;

    try {
      stmt = conn.createStatement();
      stmt.executeUpdate("insert into activeUsers (id, session) values ('" + userId + "', '" + sessionId + "');");
    } catch (SQLException ex) {
      fail("couldn't login user!", ex);
    } finally {
      close(stmt);
    }
  }

  public void logoutUser(int userId) {
    Statement stmt = null;
    
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate("delete from activeUsers where id=" + userId +");");
    } catch (SQLException ex) {
      fail("couldn't logout user id=" + userId + "!", ex);
    } finally {
      close(stmt);
    }
  }
  
  public boolean userIsActive(int userId, long sessionId) {
    Statement stmt = null;
    ResultSet rs = null;
    
    try {
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select count (*) as total from activeUsers where id=" + userId + " and session=" + sessionId);
      if (rs.next()) {
        int total = rs.getInt("total");
        assert total == 0 || total == 1;
        return (total == 1);
      }
    } catch (SQLException ex) {
      fail("Couldn't count active users...", ex);
    } finally {
      close(rs, stmt);
    }
    return false;
  }
}
