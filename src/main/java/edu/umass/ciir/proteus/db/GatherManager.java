package edu.umass.ciir.proteus.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author jfoley
 */
public class GatherManager extends CommonSQL {
  private final Connection conn;
  
  public GatherManager(Connection conn) {
    this.conn = conn;
  }
  // gather table (id, owner: USER, name)
  // gatherContents (gather_id, type, collection, resource, rating, query)
  
  public int createNewGather(String gatherName, int userId) {
    ResultSet rs = null;
    Statement stmt = null;
    
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate(
              "insert into gathers (owner, name) " +
              "values ("+userId+", "+gatherName+")");
    } catch (SQLException ex) {
      fail("Couldn't create new gather", ex);
    } finally {
      close(rs, stmt);
    }
    
    return -1;
  }
  
  public void deleteGather(int gatherId) {
    Statement stmt = null;
    
    try {
      stmt = conn.createStatement();
      stmt.executeUpdate("delete from gathers where id=" + gatherId +");");
    } catch (SQLException ex) {
      fail("couldn't logout user id=" + gatherId + "!", ex);
    } finally {
      close(stmt);
    }
  }
  
  public void addResource(int gatherId, String type, String collection, String resourceId, int rating, String query) {
    
  }
  
  
  
}
