package edu.umass.ciir.proteus.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jfoley
 */
abstract class CommonSQL {

  protected void fail(String msg, Exception ex) {
    Logger.getLogger(CommonSQL.class.getName()).log(Level.SEVERE, msg, ex);
    throw new RuntimeException(msg, ex);
  }

  protected void close(ResultSet rs, Statement stmt) {
    close(rs);
    close(stmt);
  }

  protected void close(ResultSet rs) {
    if (rs == null) {
      return;
    }

    try {
      rs.close();
    } catch (SQLException ex) {
      fail("Couldn't close ResultSet!", ex);
    }
  }

  protected void close(Statement stmt) {
    if (stmt == null) {
      return;
    }

    try {
      stmt.close();
    } catch (SQLException ex) {
      fail("Couldn't close Statement!", ex);
    }
  }

  protected void close(Connection conn) {
    if (conn == null) {
      return;
    }
    try {
      conn.close();
    } catch (SQLException ex) {
      fail("Couldn't close Connection!", ex);
    }
  }
}
