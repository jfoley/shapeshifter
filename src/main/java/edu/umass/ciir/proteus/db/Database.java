package edu.umass.ciir.proteus.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.NamingException;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author jfoley
 */
public class Database extends CommonSQL {

  // the connection to the database
  private Connection conn;
  // user administration  
  public LoginManager login;
  public GatherManager gather;

  public Database(String path) {    
    try {      
      Class.forName("org.h2.Driver");
      conn = DriverManager.getConnection("jdbc:h2:" + path);

      createTables();
      login = new LoginManager(conn);
      gather = new GatherManager(conn);

    } catch (ClassNotFoundException ex) {
      fail("couldn't find database driver", ex);
    } catch (SQLException ex) {
      fail("couldn't connect to database", ex);
    }
  }

  private void createTables() {
    Statement stmt = null;
    
    try {
      InputStream is = getClass().getResourceAsStream("/sql/createTables.sql");
      assert(is != null);
      String createTables = IOUtils.toString(is);

      stmt = conn.createStatement();
      stmt.executeUpdate(createTables);

    } catch (IOException ex) {
      fail("Couldn't find resource `sql/createTables.sql`", ex);
    } catch (SQLException ex) {
      fail("Couldn't set up initial tables...", ex);
    } finally {
      close(stmt);
    }
  }

  public static void main(String[] args) throws NamingException, SQLException, ClassNotFoundException {
    Database db = new Database("live/h2db_proteus;AUTO_SERVER=TRUE");
  }
  
}
