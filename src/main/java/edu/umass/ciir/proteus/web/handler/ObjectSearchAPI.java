package edu.umass.ciir.proteus.web.handler;

import edu.umass.ciir.proteus.backend.galago.BookSearch;
import edu.umass.ciir.proteus.backend.galago.PageSearch;
import edu.umass.ciir.proteus.objects.BaseObject;
import edu.umass.ciir.proteus.objects.ObjectCollection;
import edu.umass.ciir.proteus.objects.ObjectRetrieval;
import edu.umass.ciir.proteus.objects.ScoredObject;
import edu.umass.ciir.proteus.web.Config;
import edu.umass.ciir.proteus.web.WebRequest;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;

/**
 * This is a entry to the API for searching generic things
 * @author jfoley
 */
public class ObjectSearchAPI implements IRequestHandler {
  ObjectCollection collection;

  public ObjectSearchAPI() throws Exception {
    collection = new ObjectCollection();
    
    for(Config.CollectionConfig cfg : Config.collections) {
      System.out.println("Register "+
              (cfg.internetArchive ? "Internet Archive " : "")+
              "collection '"+cfg.name+"'");
      
      for(Config.CollectionResource res : cfg.resources) {
        String id = cfg.name+"-"+res.type;

        if("books".equals(res.type)) {
          collection.put(id, new BookSearch(res.indexPath));
        } else if("pages".equals(res.type)) {
          collection.put(id, new PageSearch(res.indexPath));
        }
        if(collection.hasRetrieval(id)) {
          System.out.println("  "+id+" as "+collection.retrieval(id));
        } else {
          System.out.println("  !! - Unsupported type "+res.type+" at "+res.indexPath);
        }
      }
    }
  }

  @Override
  public boolean shouldHandle(WebRequest request) {
    return request.path.equals("/api/search.do");
  }
  
  private void unknownSearchKind(Response response) throws IOException {
    response.setStatus(Status.BAD_REQUEST);
    response.close();
  }

  @Override
  public void handle(WebRequest request, Response response) throws IOException {
    // parse parameters
    String query = request.getString("q", "");
    int count = request.getInteger("n", 10);
    int skip = request.getInteger("skip", 0);
    String kind = request.getString("kind", "");
    
    // throw an error if they specifically ask for kind=foo, where foo is not in our map
    if(!collection.hasRetrieval(kind)) {
      unknownSearchKind(response);
      return;
    }
    
    // this is a heuristic that we might want to persist in the future because long queries are so expensive.
    // maybe with a length of a certain amount, we should drop SDM and go QL.
    if(query.length() > 1000) {
      System.err.println("Query is probably garbage. (client-side error)");
      response.setStatus(Status.BAD_REQUEST);
      response.getPrintStream().println("{error:\"I'm sorry Dave, I'm afraid I can't do that. Query is probably garbage.\"}");
      response.close();
      return;
    }
    
    // perform search
    ObjectRetrieval retrieval = collection.retrieval(kind);
    JSONArray resultsArray = new JSONArray();

    if(!query.trim().isEmpty()) {
      ArrayList<ScoredObject> results = retrieval.search(query, count, skip);
      for (ScoredObject obj : results) {
        // try to retrieve information given the id search returned
        BaseObject data = retrieval.fetch(obj.identifier);
        if (data == null) {
          continue;
        }

        // otherwise fill out and return the data object
        data.rank = obj.rank;
        data.score = obj.score;
        resultsArray.add(data.toJSON());
      }
    }
    
    //--- json response
    JSONObject json = new JSONObject();
    json.put("query", query);
    json.put("count", count);
    json.put("skip", skip);
    json.put("results", resultsArray);
    
    response.setContentType("application/json");
    PrintStream out = response.getPrintStream();
    out.println(json);
    out.close();
  }
  
}
