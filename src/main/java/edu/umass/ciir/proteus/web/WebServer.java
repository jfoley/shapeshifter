package edu.umass.ciir.proteus.web;

import edu.umass.ciir.proteus.web.handler.ContentHandler;
import edu.umass.ciir.proteus.web.handler.IRequestHandler;
import edu.umass.ciir.proteus.web.handler.ObjectSearchAPI;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.Status;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

/**
 * This is the main entry point for the web interface.
 *
 * @author jfoley
 */
public class WebServer implements Container {

  /**
   * This is a list of handlers that may be interested in the current request.
   *
   * The first IRequestHandler that responds true to shouldHandle is called to
   * handle the request.
   */
  ArrayList<IRequestHandler> handlers;

  public WebServer() {
    handlers = new ArrayList<IRequestHandler>();
  }

  /**
   * Public method for adding a IRequestHandler to the list
   *
   * @param handle
   */
  public void addHandler(IRequestHandler handle) {
    handlers.add(handle);
  }

  /**
   * Wraps this class appropriately and runs it.
   *
   * @throws IOException
   */
  protected void run() throws IOException {
    Server server = new ContainerServer(this);
    Connection connection = new SocketConnection(server);
    SocketAddress address = new InetSocketAddress(Config.serverPort);
    connection.connect(address);
  }

  /**
   * Main entry point; registers all handlers and begins running
   *
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    if(args.length == 1) {
      Config.load(args[0]);
    } else {
      Config.load();
    }
    
    WebServer ws = new WebServer();

    // register handlers
    ws.addHandler(new ObjectSearchAPI());
    // TODO: move this to class.getResource
    ws.addHandler(new ContentHandler(new String[]{"src/main/resources/web"}));

    ws.run();
  }

  /**
   * Main callback from HTTP Server library that dispatches to any
   * IRequestHandler that shouldHandle the request.
   *
   * @param request
   * @param response
   */
  @Override
  public void handle(Request request, Response response) {
    WebRequest req = new WebRequest(request);
    
    try {
      for (IRequestHandler h : handlers) {
        if (h.shouldHandle(req)) {
          h.handle(req, response);
          return;
        }
      }

      String requested = request.getPath().toString();
      if (!requested.endsWith("favicon.ico")) {
        // While working on the server, this is a useful logging feature:
        System.out.println(request);
      }
      
      // if we didn't find a handler, 404
      notFoundError(req, response);
      
    } catch (Exception ex) {
      System.err.println("Exception: "+ex);
      Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, "Exception while handling request: " + request, ex);
    }
  }

  /**
   * This is the place where we end up if there are not handlers for the given
   * page, and we decide to 404.
   *
   * @param request
   * @param response
   * @throws IOException
   */
  private void notFoundError(WebRequest request, Response response) throws IOException {
    response.setStatus(Status.NOT_FOUND);
    PrintStream body = response.getPrintStream();
    body.println("<html><body><h1>404 Error</h1></body></html>");
    body.close();
  }
}
