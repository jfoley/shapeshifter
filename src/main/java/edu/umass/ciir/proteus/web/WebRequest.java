package edu.umass.ciir.proteus.web;

import java.io.File;
import java.util.List;
import org.simpleframework.http.Cookie;
import org.simpleframework.http.Query;
import org.simpleframework.http.Request;

/**
 * A mostly-framework-oblivious wrapper around a WebRequest.
 * This is mostly because I hate that they called the parameters the "query"
 * @author jfoley
 */
public class WebRequest {
  public String method;
  public String path;
  public Query parameters;
  public List<Cookie> cookies;
  
  public WebRequest(Request request) {
    method = request.getMethod();
    path = request.getPath().toString();
    parameters = request.getQuery();
    cookies = request.getCookies();
  }
  
  public int getInteger(String key, int fallback) {
    if(parameters.containsKey(key)) {
      return parameters.getInteger(key);
    }
    return fallback;
  }
  
  public String getString(String key, String fallback) {
    if(parameters.containsKey(key)) {
      return parameters.get(key);
    }
    return fallback;
  }
  
  public File getPathFile() {
    return new File(path);
  }
}
