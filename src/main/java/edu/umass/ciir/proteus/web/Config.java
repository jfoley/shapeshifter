package edu.umass.ciir.proteus.web;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author jfoley
 */
public class Config {
  private static JSONParser parser = new JSONParser();
  private static BufferedReader reader(String path) throws FileNotFoundException {
    return new BufferedReader(new FileReader(path));
  }
    
  public static void main(String[] args) {    
    load();
  }
  public static void load() {
    load("conf.json");
  }
  public static void load(String path) {
    try {
      Object parse = parser.parse(reader(path));      
      JSONObject json = (JSONObject) parse;
      
      // parse server-wide parameters
      JSONObject serverParms = (JSONObject) json.get("server");
      serverPort = ((Number) serverParms.get("port")).intValue();
      databasePath = (String) serverParms.get("database");
      
      // parse per-collection parameters
      collections = new ArrayList<CollectionConfig>();
      for(Object obj : (JSONArray) json.get("collections")) {
        collections.add(new CollectionConfig((JSONObject) obj));        
      }
    } catch (FileNotFoundException fnfe) {
      throw new RuntimeException("Couldn't find a ``conf.json`` file to configure the server. Abort.");
    } catch (IOException ioe) {
      throw new RuntimeException("Issue reading ``conf.json`` "+ioe);
    } catch (ParseException pe) {
      throw new RuntimeException("Issue parsing ``conf.json`` "+pe);
    }
  }

  public static int serverPort = 8080;
  private static String databasePath;
  public static ArrayList<CollectionConfig> collections = new ArrayList<CollectionConfig>();
  
  public static class CollectionResource {
    public final String type;
    public final String indexPath;
    public CollectionResource(JSONObject input) {
      type = (String) input.get("type");
      assert(type != null);
      indexPath = (String) input.get("indexPath");
      assert(indexPath != null);
    }
  }
  
  public static class CollectionConfig {
    public final String name;
    public final boolean internetArchive;
    public final ArrayList<CollectionResource> resources;
    public CollectionConfig(JSONObject input) {
      name = (String) input.get("name");
      assert(name != null);
      internetArchive = ((Boolean) input.get("internetArchive")).booleanValue();
      
      resources = new ArrayList<CollectionResource>();
      for(Object obj : (JSONArray) input.get("resources")) {
        resources.add(new CollectionResource((JSONObject) obj));
      }
    }
  }
}
