package edu.umass.ciir.proteus.web.handler;

import edu.umass.ciir.proteus.db.Database;
import edu.umass.ciir.proteus.web.WebRequest;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Set;
import org.simpleframework.http.Cookie;
import org.simpleframework.http.Response;

/**
 *
 * @author jfoley
 */
public class LoginHandler implements IRequestHandler {
  // the api pages that this responds to
  public static final String loginPath = "/login.do";
  public static final String createUserPath = "/createUser.do";
  public static final Set<String> acceptablePaths = new HashSet<String>();
  static {
    acceptablePaths.add(loginPath);
    acceptablePaths.add(createUserPath);
  }
  
  // the name of the session cookie
  public static final String sessionCookieName = "proteusSession";
  private final Database db;
  
  public LoginHandler(Database db) {
    this.db = db;
  }

  @Override
  public boolean shouldHandle(WebRequest request) {
    return acceptablePaths.contains(request.path);
  }

  @Override
  public void handle(WebRequest request, Response response) throws IOException {
    if(request.path.equals(loginPath)) {
      login(request, response);
    } else if(request.path.equals(createUserPath)) {
      createUser(request, response);
    } else {
      throw new RuntimeException("Logic error in LoginHandler -> no handle for "+request);
    }
  }
  
  private Cookie newSessionCookie(int userId) {
    SecureRandom rand = new SecureRandom();
    rand.setSeed(userId+System.currentTimeMillis());
    long sessionId = rand.nextLong();
    db.login.loginUser(userId, sessionId);
    Cookie session = new Cookie(sessionCookieName, Long.toHexString(sessionId));
    return session;
  }

  private void login(WebRequest request, Response response) throws IOException {
    response.setCookie("proteusSession", "this-is-my-session-key");
    
    response.getPrintStream().println("TODO: login");
    response.close();

  }

  private void createUser(WebRequest request, Response response) throws IOException {
    response.getPrintStream().println("TODO: createUser");
    response.close();
  }
  
}
