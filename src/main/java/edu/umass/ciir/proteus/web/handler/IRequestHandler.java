package edu.umass.ciir.proteus.web.handler;

import edu.umass.ciir.proteus.web.WebRequest;
import java.io.IOException;
import org.simpleframework.http.Response;

/**
 * This is an interface that is mildly equivalent to a Servlet.
 * Given a request, it determines whether or not it has a valid response.
 * @author jfoley
 */
public interface IRequestHandler {
  /**
   * Returns true if this handler has a response for the given request.
   * @param request
   * @return 
   */
  public boolean shouldHandle(WebRequest request);
  /**
   * Given the request, write the response to the response object.
   * @param request
   * @param response
   * @throws IOException 
   */
  public void handle(WebRequest request, Response response) throws IOException;
}
