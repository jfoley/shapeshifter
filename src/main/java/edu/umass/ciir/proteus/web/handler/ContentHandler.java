package edu.umass.ciir.proteus.web.handler;

import edu.umass.ciir.proteus.web.WebRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.h2.util.IOUtils;
import org.simpleframework.http.Response;

/**
 * The following is a handler for serving files directly.
 *
 * For now we don't do any caching of our own, but depend on the
 * filesystem/OS/kernel to do that, and avoid the need to restart while editing
 * html assets.
 *
 * @author jfoley
 */
public class ContentHandler implements IRequestHandler {

  Set<String> basePaths;
  public static String DefaultPage = "index.html";

  /**
   * Convenience method to invoke with an String[] of paths.
   *
   * @param dirPaths
   * @throws IOException
   */
  public ContentHandler(String[] dirPaths) throws IOException {
    File[] asFiles = new File[dirPaths.length];
    for (int i = 0; i < asFiles.length; i++) {
      asFiles[i] = new File(dirPaths[i]);
    }
    init(asFiles);
  }

  /**
   * Serve all paths that are sub-paths of these directories.
   *
   * @param dirFiles
   * @throws IOException
   */
  public ContentHandler(File[] dirFiles) throws IOException {
    init(dirFiles);
  }

  private void init(File[] dirFiles) throws IOException {
    List<File> files = Arrays.asList(dirFiles);
    basePaths = new HashSet<String>();
    for (File fp : files) {
      System.out.println("Try to add path: " + fp + " from " + System.getProperty("user.dir"));
      if (fp.exists() && fp.isDirectory()) {
        String base = fp.getCanonicalPath() + File.separator;
        System.out.println("FileHandler serving: " + base);
        basePaths.add(base);
      } else {
        throw new RuntimeException("Cannot find directory " + fp.getCanonicalPath());
      }
    }
  }

  private File find(String path) {
    // output a warning if someone's doing something bad
    if (path.contains("..")) {
      System.out.println("Somebody requested an up-directory link -> do no evil.");
      return null;
    }
    // redirect default request to index.html if possible
    if (path.endsWith("/")) {
      path += DefaultPage;
    }

    // see if this file exists and if it is a child of any base path
    for (String base : basePaths) {
      try {
        File demandFile = new File(base + path);
        String demandPath = demandFile.getCanonicalPath();
        if (demandPath.startsWith(base) && demandFile.exists() && demandFile.isFile()) {
          return demandFile;
        }
      } catch (IOException ex) {
        // treat as couldn't find
      }
    }

    // didn't find anything
    return null;
  }

  @Override
  public boolean shouldHandle(WebRequest request) {
    return find(request.path) != null;
  }

  private String extension(File f) {
    if (f.isDirectory()) {
      return "";
    }

    String name = f.getName();
    if(name.contains(".")) {
      int dotPt = name.lastIndexOf(".") + 1;
      return name.substring(dotPt);
    } 
    return "";
  }

  /**
   * Translate an extension into a mime type for the Content-Type field
   * response.
   *
   * https://en.wikipedia.org/wiki/Internet_media_type
   *
   * @param ext
   * @return
   */
  private String mimeType(String ext) {
    if (ext.equals("css")) {
      return "text/css";
    }
    if (ext.equals("js")) {
      return "text/javascript";
    }
    if (ext.equals("html")) {
      return "text/html";
    }
    if (ext.equals("ico") || ext.equals("gif") || ext.equals("png") || ext.equals("jpeg")) {
      return String.format("image/%s", ext);
    }
    if (ext.equals("jpg")) {
      return "image/jpeg";
    }
    return null;
  }

  @Override
  public void handle(WebRequest request, Response response) throws IOException {
    File resource = find(request.path);
    String mtype = mimeType(extension(resource));
    if (mtype != null) {
      response.setContentType(mtype);
    }

    // use commons-io to copy files
    FileInputStream fis = new FileInputStream(resource);
    IOUtils.copy(fis, response.getOutputStream());
    
    fis.close();
    response.close();
  }
}
