// BSD License (http://lemurproject.org/galago-license)
package edu.umass.ciir.proteus.objects;

import java.util.Map;

/**
 *
 * @author jfoley
 */
public class MetadataUtility {
  
  public static String tryGetString(Map<String, String> metadata, String key) {
    if (metadata.containsKey(key)) {
      return metadata.get(key);
    }
    return null;
  }
  
  public static int tryGetInt(Map<String,String> metadata, String key) {
    if(!metadata.containsKey(key))
      return -1;
    
    try {
      return Integer.parseInt(metadata.get(key));
    } catch (NumberFormatException nfe) {
      return -1;
    }
  }
}
