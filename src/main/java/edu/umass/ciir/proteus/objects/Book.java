package edu.umass.ciir.proteus.objects;

import java.io.IOException;
import org.json.simple.JSONObject;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.parse.Document.DocumentComponents;
import org.lemurproject.galago.core.retrieval.Retrieval;

/**
 *
 * @author jfoley
 */
public class Book extends BaseObject {

  private static final int TextSize = 1024;
  private static DocumentComponents fetchParameters = _fetchParameters();
  public String summary;
  public String subject;
  public String creator;
  public int numPages = -1;
  public int year = -1;

  /**
   * Fetch metadata about a book to fill out the object
   */
  public Book(String id, Retrieval galago) throws IOException {
    super("book", id);
    
    Document d = galago.getDocument(identifier, fetchParameters);
    if (d == null) {
      // since the retrieval gave us this id, not having it is exceptional
      throw new IOException("No such document");
    }

    text = d.text;

    title = MetadataUtility.tryGetString(d.metadata, "title");
    summary = MetadataUtility.tryGetString(d.metadata, "summary");
    subject = MetadataUtility.tryGetString(d.metadata, "subject");
    creator = MetadataUtility.tryGetString(d.metadata, "creator");

    numPages = MetadataUtility.tryGetInt(d.metadata, "numPages");
    year = MetadataUtility.tryGetInt(d.metadata, "date");
  }

  @Override
  public JSONObject convertToJSON() throws IOException {
    JSONObject obj = new JSONObject();

    setBaseFields(obj);
    obj.put("summary", summary);
    obj.put("subject", subject);
    obj.put("creator", creator);
    obj.put("numPages", numPages);
    obj.put("year", year);

    return obj;

  }

  /**
   * These are the Galago parameters for reading the document from the corpus.
   */
  private static DocumentComponents _fetchParameters() {
    DocumentComponents dc = new DocumentComponents();
    dc.metadata = true;

    // read only part of the text
    dc.text = true;
    dc.subTextStart = 0;
    dc.subTextLen = TextSize;

    // don't run through TagTokenizer on load
    dc.tokenize = false;
    return dc;
  }
}
