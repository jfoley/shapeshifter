package edu.umass.ciir.proteus.objects;

import java.io.IOException;
import org.json.simple.JSONObject;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.parse.Document.DocumentComponents;
import org.lemurproject.galago.core.retrieval.Retrieval;

/**
 * This represents the page of a book.
 * @author jfoley
 */
public class BookPage extends BaseObject {
  private static DocumentComponents fetchParameters = _fetchParameters();
  
  private final String bookId;
  private final int pageNum;
  private String summary;
  private String subject;
  private String creator;
  private int year;
  
  public BookPage(String id, Retrieval galago) throws IOException {
    super("book-page", id);
    String[] info = id.split("_");
    assert(info.length == 2);
    bookId = info[0];
    pageNum = Integer.parseInt(info[1]);
    
    Document d = galago.getDocument(identifier, fetchParameters);
    if (d == null) {
      // since the retrieval gave us this id, not having it is exceptional
      throw new IOException("No such document");
    }
    
    text = d.text;

    title = MetadataUtility.tryGetString(d.metadata, "title");
    summary = MetadataUtility.tryGetString(d.metadata, "summary");
    subject = MetadataUtility.tryGetString(d.metadata, "subject");
    creator = MetadataUtility.tryGetString(d.metadata, "creator");
    year = MetadataUtility.tryGetInt(d.metadata, "date");
  }

  @Override
  protected JSONObject convertToJSON() throws IOException {
    JSONObject obj = new JSONObject();
    
    setBaseFields(obj);
    obj.put("bookId", bookId);
    obj.put("pageNum", pageNum);
    obj.put("summary", summary);
    obj.put("subject", subject);
    obj.put("creator", creator);
    obj.put("year", year);
    
    return obj;
  }

  private static DocumentComponents _fetchParameters() {
    DocumentComponents dc = new Document.DocumentComponents();
    dc.metadata = true;
    // since pages should be small, we read the whole thing
    dc.text = true;
    // don't run through TagTokenizer on load
    dc.tokenize = false;
    return dc;
  }
}
