package edu.umass.ciir.proteus.objects;

import java.util.HashMap;
import java.util.Map;

/**
 * A ObjectCollection is a collection of ObjectRetrieval for each type.
 * @author jfoley
 */
public class ObjectCollection {
  Map<String, ObjectRetrieval> retrievals;

  public ObjectCollection() {
    this.retrievals = new HashMap<String, ObjectRetrieval>();
  }
  
  public void put(String kind, ObjectRetrieval retr) {
    //TODO some sort of merged retrieval
    assert(!hasRetrieval(kind));
    retr.setCollection(this);
    retrievals.put(kind, retr);
  }
  
  public boolean hasRetrieval(String type) {
    return retrievals.containsKey(type);
  }
  
  public ObjectRetrieval retrieval(String type) {
    if(!hasRetrieval(type))
      throw new RuntimeException("Collection does not contain type `"+type+"'");
    
    return retrievals.get(type);
  }
  
  public BaseObject fetch(String type, String identifier) {
    return retrieval(type).fetch(identifier);
  }
}
