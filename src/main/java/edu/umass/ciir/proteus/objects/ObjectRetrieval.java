package edu.umass.ciir.proteus.objects;

import java.util.ArrayList;

/**
 * A generic interface to accessing BaseObject types.
 * @author jfoley
 */
public abstract class ObjectRetrieval {
  // for allowing access of other types during fetch
  public ObjectCollection collection;
  
  public static ArrayList<ScoredObject> NoResults = new ArrayList<ScoredObject>();
  public abstract ArrayList<ScoredObject> search(String query, int count, int skip);
  public abstract BaseObject fetch(String identifier);

  public void setCollection(ObjectCollection parent) {
    collection = parent;
  }
}
