package edu.umass.ciir.proteus.objects;

/**
 *
 * @author jfoley
 */
public class ScoredObject {
  public String identifier;
  public double score;
  public int rank;
}
