// BSD License (http://lemurproject.org/galago-license)
package edu.umass.ciir.proteus.objects;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author jfoley
 */
public abstract class BaseObject {

  // the four mandatory pieces of information
  public String type;
  public String identifier;
  public String title;
  public String text;
  
  // the two pieces of information we can bubble up from the search engine
  public double score = Double.NaN;
  public int rank = -1;
  
  protected BaseObject(String type, String identifier) {
    this.type = type;
    this.identifier = identifier;
  }

  /**
   * Encode this object as JSON for returning to client side.
   *
   * This is where it is implemented - subclasses can throw IOExceptions, they
   * will be turned into Javascript 'null' values.
   *
   * @return JSONObject containing interesting fields
   */
  protected abstract JSONObject convertToJSON() throws IOException;

  /**
   * Prevent copy & pasta errors in subclasses.
   * @param json 
   */
  protected void setBaseFields(JSONObject json) {
    assert type != null;
    assert identifier != null;
    assert rank != -1;

    json.put("type", type);
    json.put("identifier", identifier);
    json.put("title", title);
    json.put("text", text);
    json.put("score", score);
    json.put("rank", rank);
  }
  
  /**
   * Wrapper method to turn any IOExceptions during generation into null values.
   * @return 
   */
  public final JSONObject toJSON() {
    try {
      return convertToJSON();
    } catch (IOException ex) {
      Logger.getLogger(BaseObject.class.getName()).log(Level.SEVERE, "Couldn't convert '" + identifier + "' to JSON!", ex);
    }
    return null;
  }
}
