ShapeShifter
============

Proteus: The Third Time's a Charm

TODO: instructions for setup

build
---

    mvn package

start the server
---

Invoke the maven run plugin as follows, then navigate to [http://localhost:8080](http://localhost:8080)

    mvn exec:java
    
find the source code
---

The server source code is located among ``src/main/java`` as is the convention, and the HTML and JS sources for the web app are located in ``src/main/resources`` so they get dragged along when maven builds a jar. This allows for easier deployment (our production server won't need maven installed to run itself.)

configure things
---

There is a configuration file (TBD)
